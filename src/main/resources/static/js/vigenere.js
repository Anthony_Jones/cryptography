var customActions = {
    crypt: {method: 'POST', url: '/vigenere/crypt'},
    decrypt: {method: 'POST', url: '/vigenere/decrypt'}
};
var replaceApi = Vue.resource('/vigenere', {}, customActions);

Vue.component('crypt-form', {
    props: [],
    data: function () {
        return {
            text: '',
            lang: '',
            crypt_key: '',
            max: '',
            result: 'Crypt or decrypt some text to view result',
            message: ''
        }
    },
    template: '<div class="uk-card-default">' +
        '<input id="text" type="text" placeholder="Enter text" v-model="text">' +
        '<div><label for=""><input id="rus" name="language" type="radio" value="rus" v-model="lang">Russian</label> ' +
        '<label for=""><input id="eng" checked="checked" name="language" type="radio" value="eng" v-model="lang">English</label></div>' +
        '<div><input id="key" type="key" placeholder="Enter key" v-model="crypt_key"></div>' +
        '<div><input type="button" value="Crypt" @click="crypt">' +
        '<input type="button" value="Dectypt" @click="decrypt">' +
        '<div :result="result" class="result">{{result}}</div>' +
        '<div :message="message" class="message">{{message}}</div>' +
        '</div></div>',

    methods: {
        crypt: function () {
            replaceApi.crypt({text: this.text, key: this.crypt_key, lang: this.lang}).then(result =>
                result.json().then(data => {
                        if (parseInt(data.status)) {
                            this.result = data.result;
                            this.message = '';
                        } else {
                            this.result = '';
                            this.message = data.message;
                        }
                    }
                )
            )
        },
        decrypt: function () {
            replaceApi.decrypt({text: this.text, key: this.crypt_key, lang: this.lang}).then(result =>
                result.json().then(data => {
                    if (parseInt(data.status)) {
                        this.result = data.result;
                        this.message = '';
                    } else {
                        this.result = '';
                        this.message = data.message;
                    }
                    }
                )
            )
        }
    }
});
