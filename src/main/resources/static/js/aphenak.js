var customActions = {
    crypt: {method: 'POST', url: '/aphenak/crypt'},
    decrypt: {method: 'POST', url: '/aphenak/decrypt'},
    attack: {method: 'POST', url: '/aphenak/attack'}
};
var replaceApi = Vue.resource('/aphenak', {}, customActions);
Vue.component('crypt-form', {
    props: [],
    data: function () {
        return {
            text: 'завтра',
            a11: '1',
            a12: '1',
            a21: '32',
            a22: '1',
            s11: '1',
            s12: '15',
            result: '',
            message: 'Crypt or decrypt some text to view result',
            lang: 'rus'
        }
    },
    template: '<div class="uk-card-default">' +
        '<textarea class="result" id="text" type="text" placeholder="Enter text" v-model="text"></textarea>' +
        '<div>Matrix A'+
        '<div class="matrix2">' +
        '<input class="element" id="a11" type="key" placeholder="a11" v-model="a11">' +
        '<input class="element"  id="a12" type="key" placeholder="a12" v-model="a12">' +
        '<input class="element" id="a21" type="key" placeholder="a21" v-model="a21">' +
        '<input  class="element" id="a22" type="key" placeholder="a22" v-model="a22">' +
        '</div>' +
        '</div>'+
        '<div>Vector S'+
        '<div>' +
        '<input class="element"  id="s11" type="key" placeholder="s11" v-model="s11">' +
        '<input class="element"  id="s12" type="key" placeholder="s12" v-model="s12">' +
        '</div>' +
        '</div>'+
        '<label for=""><input id="rus" name="language" type="radio" value="rus" v-model="lang">Russian</label> '+
        '<label checked for=""><input id="eng" name="language" type="radio" value="eng" v-model="lang">English</label>'+
        '<div><input type="button" value="Crypt" @click="crypt">' +
        '<input type="button" value="Dectypt" @click="decrypt">' +
        '<input type="button" value="Attack" @click="attack">' +
        '<textarea readonly :result="result" class="result">{{result}}</textarea>' +
        '<div :message="message" class="message">{{message}}</div>' +
        '</div></div>',

    methods: {
        crypt: function () {
            replaceApi.crypt({
                text: this.text,
                a11: this.a11,
                a12: this.a12,
                a21: this.a21,
                a22: this.a22,
                s11: this.s11,
                s12: this.s12,
                lang: this.lang
            }).then(result =>
                result.json().then(data => {
                        if (parseInt(data.status)) {
                            this.result = data.result;
                            this.message = '';
                        } else {
                            this.result = '';
                            this.message = data.message;
                        }
                    }
                )
            )
        },
        decrypt: function () {
            replaceApi.decrypt({
                text: this.text,
                a11: this.a11,
                a12: this.a12,
                a21: this.a21,
                a22: this.a22,
                s11: this.s11,
                s12: this.s12,
                lang: this.lang
            }).then(result =>
                result.json().then(data => {
                        if (parseInt(data.status)) {
                            this.result = data.result;
                            this.message = '';
                        } else {
                            this.result = '';
                            this.message = data.message;
                        }
                    }
                )
            )
        },
        attack: function () {
            replaceApi.attack({text: this.text, lang: this.lang}).then(result =>
                result.json().then(data => {
                        if (parseInt(data.status)) {
                            this.result = data.result;
                            this.message = '';
                        } else {
                            this.result = '';
                            this.message = data.message;
                        }
                    }
                )
            )
        }
    }
});
