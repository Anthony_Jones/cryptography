var customActions = {
    crypt: {method: 'POST', url: '/permutations/crypt'},
    decrypt: {method: 'POST', url: '/permutations/decrypt'},
    attack: {method: 'POST', url: '/permutations/attack'}
};
var replaceApi = Vue.resource('/permutations', {}, customActions);

Vue.component('crypt-form', {
    props: [],
    data: function () {
        return {
            text: '',
            crypt_key: '',
            result: 'Crypt or decrypt some text to view result',
            message: ''
        }
    },
    template: '<div class="uk-card-default">' +
        '<textarea id="text" type="text" placeholder="Enter text" v-model="text"></textarea>' +
        '<div><input id="key" type="key" placeholder="Enter key" v-model="crypt_key"></div>' +
        '<div><input type="button" value="Crypt" @click="crypt">' +
        '<input type="button" value="Dectypt" @click="decrypt">' +
        '<input type="button" value="Attack" @click="attack">' +
        '<pre :result="result" class="result">{{result}}</pre>' +
        '<div :message="message" class="message">{{message}}</div>' +
        '</div></div>',

    methods: {
        crypt: function () {
            replaceApi.crypt({text: this.text, key: this.crypt_key, lang: this.lang}).then(result =>
                result.json().then(data => {
                        if (parseInt(data.status)) {
                            this.result = data.result;
                            this.message = '';
                        } else {
                            this.result = '';
                            this.message = data.message;
                        }
                    }
                )
            )
        },
        decrypt: function () {
            replaceApi.decrypt({text: this.text, key: this.crypt_key, lang: this.lang}).then(result =>
                result.json().then(data => {
                        if (parseInt(data.status)) {
                            this.result = data.result;
                            this.message = '';
                        } else {
                            this.result = '';
                            this.message = data.message;
                        }
                    }
                )
            )
        },
        attack: function () {
            replaceApi.attack({text: this.text, key: this.crypt_key, lang: this.lang}).then(result =>
                result.json().then(data => {
                        if (parseInt(data.status)) {
                            this.result = data.result;
                            this.message = '';
                        } else {
                            this.result = '';
                            this.message = data.message;
                        }
                    }
                )
            )
        }
    }
});
