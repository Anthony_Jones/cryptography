var customActions = {
    crypt: {method: 'POST', url: '/caesar/crypt'},
    decrypt: {method: 'POST', url: '/caesar/decrypt'},
    freq: {method: 'POST', url: '/caesar/freq'},
    attack: {method: 'POST', url: '/caesar/attack'},
};
var caesarApi = Vue.resource('/caesar', {}, customActions);

Vue.component('crypt-form', {
    props: [],
    data: function () {
        return {
            text: '',
            lang: '',
            crypt_key: '',
            max: '',
            result: 'Crypt or decrypt some text to view result'
        }
    },
    template: '<div class="uk-card-default">' +
        '<input id="text" type="text" placeholder="Enter text" v-model="text">'+
        '<label for=""><input id="rus" name="language" type="radio" value="rus" v-model="lang">Russian</label> '+
        '<label for=""><input id="eng" checked="checked" name="language" type="radio" value="eng" v-model="lang">English</label>'+
        '<input id="key" class="uk-range" type="range" v-model="crypt_key" value="1" min="1" :max="26" step="1">'+
        '<input type="button" value="Crypt" @click="crypt">' +
        '<input type="button" value="Frequency" @click="freq">' +
        '<input type="button" value="Dectypt" @click="decrypt">' +
        '<input type="button" value="Attack" @click="attack">' +
        '<div :result="result" class="result">{{result}}</div>'+
        '</div>',
    methods: {
        crypt: function () {
            caesarApi.crypt({text: this.text, key: this.crypt_key, lang: this.lang}).then(result =>
                result.json().then(data => {
                        this.result = data;
                    }
                )
            )
        },
        freq: function () {
            caesarApi.freq({text: this.text, key: this.crypt_key, lang: this.lang}).then(result =>
                result.json().then(data => {
                        this.result = data;
                    }
                )
            )
        },
        decrypt: function () {
            caesarApi.decrypt({text: this.text, key: this.crypt_key, lang: this.lang}).then(result =>
                result.json().then(data => {
                        this.result = data;
                    }
                )
            )
        },
        attack: function () {
            caesarApi.attack({text: this.text, key: this.crypt_key, lang: this.lang}).then(result =>
                result.json().then(data => {
                        this.result = data;
                    }
                )
            )
        },
    }
});