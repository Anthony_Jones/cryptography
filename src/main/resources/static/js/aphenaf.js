var customActions = {
    crypt: {method: 'POST', url: '/aphenaf/crypt'},
    decrypt: {method: 'POST', url: '/aphenaf/decrypt'},
    attack: {method: 'POST', url: '/aphenaf/attack'}
};
var replaceApi = Vue.resource('/aphenaf', {}, customActions);

Vue.component('crypt-form', {
    props: [],
    data: function () {
        return {
            text: '',
            crypt_keya: '',
            crypt_keyb: '',
            result: 'Crypt or decrypt some text to view result',
            message: '',
            lang: ''
        }
    },
    template: '<div class="uk-card-default">' +
        '<textarea id="text" type="text" placeholder="Enter text" v-model="text"></textarea>' +
        '<div><input id="keya" type="key" placeholder="Enter a" v-model="crypt_keya"></div>' +
        '<div><input id="keyb" type="key" placeholder="Enter b" v-model="crypt_keyb"></div>' +
        '<label for=""><input id="rus" name="language" type="radio" value="rus" v-model="lang">Russian</label> '+
        '<label for=""><input id="eng" checked="checked" name="language" type="radio" value="eng" v-model="lang">English</label>'+
        '<div><input type="button" value="Crypt" @click="crypt">' +
        '<input type="button" value="Dectypt" @click="decrypt">' +
        '<input type="button" value="Attack" @click="attack">' +
        '<textarea :result="result" class="result">{{result}}</textarea>' +
        '<div :message="message" class="message">{{message}}</div>' +
        '</div></div>',

    methods: {
        crypt: function () {
            replaceApi.crypt({
                text: this.text,
                keya: this.crypt_keya,
                keyb: this.crypt_keyb,
                lang: this.lang
            }).then(result =>
                result.json().then(data => {
                        if (parseInt(data.status)) {
                            this.result = data.result;
                            this.message = '';
                        } else {
                            this.result = '';
                            this.message = data.message;
                        }
                    }
                )
            )
        },
        decrypt: function () {
            replaceApi.decrypt({
                text: this.text,
                keya: this.crypt_keya,
                keyb: this.crypt_keyb,
                lang: this.lang
            }).then(result =>
                result.json().then(data => {
                        if (parseInt(data.status)) {
                            this.result = data.result;
                            this.message = '';
                        } else {
                            this.result = '';
                            this.message = data.message;
                        }
                    }
                )
            )
        },
        attack: function () {
            replaceApi.attack({text: this.text, key: this.crypt_key, lang: this.lang}).then(result =>
                result.json().then(data => {
                        if (parseInt(data.status)) {
                            this.result = data.result;
                            this.message = '';
                        } else {
                            this.result = '';
                            this.message = data.message;
                        }
                    }
                )
            )
        }
    }
});
