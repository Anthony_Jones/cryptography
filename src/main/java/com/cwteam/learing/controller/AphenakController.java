package com.cwteam.learing.controller;

import com.cwteam.learing.exception.ValidationException;
import com.cwteam.learing.helpers.MatrixDouble;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.cwteam.learing.helpers.CryptHelper.*;

@RestController
@RequestMapping("/aphenak")
public class AphenakController {


    private char[] alphabet;
    private int alphabetSize;
    private HashMap<Character, Integer> alphabetHashMap;

    private MatrixDouble matrix;
    private MatrixDouble vector;

    private String text;

    private MatrixDouble bigramMatrix;

    private LinkedHashMap<Integer,ArrayList<Integer>> keysRus = new LinkedHashMap<>() {{
        ArrayList<Integer> keyB = new ArrayList<>() {{
            for (int i = 0; i < RusSize; ++i) {
                add(i);
            }
        }};
        for (Integer i :
                RusSimpleDigits) {
            put(i,keyB);
        }
    }};

    private LinkedHashMap<Integer,ArrayList<Integer>> keysEng = new LinkedHashMap<>() {{
        ArrayList<Integer> keyB = new ArrayList<>() {{
            for (int i = 0; i < EngSize; ++i) {
                add(i);
            }
        }};
        for (Integer i :
                EngSimpleDigits) {
            put(i,keyB);
        }
    }};

    public void checkData(@RequestBody Map<String, String> data) throws ValidationException {
        if (data.get("text").isEmpty()) {
           throw new ValidationException("Enter text");
        } else {
            text = data.get("text").toLowerCase().replaceAll("\\s+","");
        }
        if (data.get("lang").equals("rus")) {
            alphabetSize = RusSize;
            alphabet = russianArray;
            alphabetHashMap = russianHashMap;
            text = text.replaceAll("[^А-я]","");
        } else {
            alphabetSize = EngSize;
            alphabet = englishArray;
            alphabetHashMap = englishHashMap;
            text = text.replaceAll("[^A-z]","");

        }
        MatrixDouble temp = new MatrixDouble(
                new ArrayList<>() {{
                    add(Double.valueOf(data.get("a11")));
                    add(Double.valueOf(data.get("a12")));
                    add(Double.valueOf(data.get("a21")));
                    add(Double.valueOf(data.get("a22")));
                }},
                2,
                2
        );
        for (Double i :
             temp) {
            if(i < 0 || i > alphabetSize) {
                throw new ValidationException("Matrix elements must be in [0, "+ alphabetSize+")");
            }
        }
        if(gcd(Math.abs(temp.det().intValue()) % alphabetSize, alphabetSize) != 1) {
            throw new ValidationException("Matrix has to have inverse");
        } else {
            this.matrix = temp;
        }
        this.vector = new MatrixDouble(new ArrayList<>() {{
            add(Double.valueOf(data.get("s11")));
            add(Double.valueOf(data.get("s12")));
        }},2,1);
        for (Double i :
                vector) {
            if(i < 0 || i > alphabetSize) {
                throw new ValidationException("Vector elements must be in [0, "+ alphabetSize+")");
            }
        }
        bigramMatrix = new MatrixDouble(2, text.length() / 2 + text.length() % 2 == 0 ? 0 : 1 );
        char [] textArray = text.toCharArray();

        for (int i = 0, j = 0; i < text.length(); i+=2, ++j) {
            bigramMatrix.set(Double.valueOf(alphabetHashMap.get(textArray[i])),0,j);
        }
        for (int i = 1, j = 0 ; i <  text.length(); i+=2, ++j) {
            bigramMatrix.set(Double.valueOf(alphabetHashMap.get(textArray[i])),1,j);
        }
        if(text.length() % 2 != 0) {
            bigramMatrix.set((double) -1,1,text.length() / 2);
        }
    }
    int gcd (int a, int b) {
        return b != 0 ? gcd (b, a % b) : a;
    }

    private HashMap<String, String> errorMessage(String text) {
        return new HashMap<>() {{
            put("message", text);
            put("status", "0");
        }};
    }

    private HashMap<String, String> successMessage(String text) {
        return new HashMap<>() {{
            put("message", text);
            put("status", "1");
        }};
    }

    @PostMapping("/crypt")
    public Map<String, String> crypt(@RequestBody Map<String, String> textAndKey) throws ValidationException {
        try {
            checkData(textAndKey);
        } catch (ValidationException e) {
            return errorMessage(e.getMessage());
        }
        String result = cipher();
        if(text.length() % 2 != 0) {
            result = result.replaceFirst(".$","");
        }
        String finalResult = result;
        return new HashMap<String, String>() {{
            put("result", finalResult);
            put("status", "1");
        }};
    }

    @PostMapping("/decrypt")
    public Map<String, String> decrypt(@RequestBody Map<String, String> textAndKey) {
        try {
            checkData(textAndKey);
        } catch (ValidationException e) {
            return errorMessage(e.getMessage());
        }
        String result = decipher();
        if(text.length() % 2 != 0) {
            result = result.replaceFirst(".$","");
        }

        String finalResult = result;
        return new HashMap<String, String>() {{
            put("result", finalResult);
            put("status", "1");
        }};
    }

    @PostMapping("/attack")
    private Map<String, String> attack(@RequestBody Map<String, String> requestData) {

        return new HashMap<>() {{
            put("result", "");
            put("status", "1");
        }};
    }

    private String cipher() {
        MatrixDouble result = matrix.mul(bigramMatrix);
        for (int i = 0; i < result.columns(); i++) {
            result.set((result.get(0, i) + vector.get(0,0)) % alphabetSize ,0, i);
        }
        for (int i = 0; i < result.columns(); i++) {
            result.set((result.get(1, i) + vector.get(1,0)) % alphabetSize,1,i);
        }
        StringBuilder resultString  = new StringBuilder();
        for (int i = 0; i < result.columns(); i++) {
            resultString.append(alphabet[result.get(0, i).intValue()]);
            resultString.append(alphabet[result.get(1, i).intValue()]);
        }
        return resultString.toString();
    }

    private String decipher() {
        MatrixDouble inv = matrix.inv(alphabetSize);
        MatrixDouble invVector = inv.mul(vector);
        invVector.set((-1 * invVector.get(0,0)) % alphabetSize, 0,0);
        invVector.set((-1 * invVector.get(1,0)) % alphabetSize, 1,0);


        StringBuilder resultString = new StringBuilder();
        MatrixDouble result = inv.mul(bigramMatrix);
        for (int i = 0; i < result.columns(); i++) {
            result.set((result.get(0, i) + invVector.get(0,0)) % alphabetSize ,0, i);
        }
        for (int i = 0; i < result.columns(); i++) {
            result.set((result.get(1, i) + invVector.get(1,0)) % alphabetSize,1,i);
        }
        for (int i = 0; i < result.columns(); i++) {
            resultString.append(alphabet[result.get(0, i).intValue()]);
            resultString.append(alphabet[result.get(1, i).intValue()]);
        }
        return resultString.toString();
    }
}
