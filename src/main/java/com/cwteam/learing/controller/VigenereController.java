package com.cwteam.learing.controller;

import com.cwteam.learing.helpers.CryptHelper;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/vigenere")
public class VigenereController {


    @PostMapping("/crypt")
    public Map<String, String> crypt(@RequestBody Map<String, String> textAndKey)  {
        String keyString = textAndKey.get("key").trim().toLowerCase();
        char[] key = keyString.toCharArray();
        String textString = textAndKey.get("text").trim().toLowerCase().replaceAll("[\\s,;.-]","");
        char[] text = textString.toCharArray();
        if(text.length == 0) {
            return new HashMap<String, String>() {{
                put("message", "Введите текст");
                put("status", "0");
            }};
        }
        String lang = textAndKey.get("lang");

        StringBuilder result = new StringBuilder();
        HashMap<Character, Integer> array = null;
        int alphabetSize = 0;
        if(lang.equals("rus")) {
            alphabetSize = CryptHelper.russian.length();
            if(!textAndKey.get("key").trim().toLowerCase().matches("[а-я]+")) {
                return new HashMap<String, String>() {{
                    put("message", "Введите ключ на русском языке");
                    put("status", "0");
                }};
            }
            array = CryptHelper.russianHashMap;
        } else {
            alphabetSize = CryptHelper.english.length();
            if(!textAndKey.get("key").trim().toLowerCase().matches("[a-z]+")) {
                return new HashMap<String, String>() {{
                    put("message", "Введите ключ на английском языке");
                    put("status", "0");
                }};
            }
            array = CryptHelper.englishHashMap;
        }
        int keyLength = key.length;
        int additionalLength = text.length % keyLength;
        if(additionalLength != 0) {
            int Z = keyLength / additionalLength;
            int ost = keyLength % additionalLength;
            for (int i = 0; i < Z; i++) {
                keyString += keyString;
            }
            keyString += keyString.substring(0, ost);
            key = keyString.toCharArray();
        }
        int j = 0;

        for (char i :
                text) {
            int keyCh = i;
            if (!(String.valueOf(i).matches("[a-z]") && lang.equals("rus"))
                    && !(String.valueOf(i).matches("[а-я]") && lang.equals("eng"))
                    && !Character.isDigit(i)
                    && !Character.isWhitespace(i)) {
                keyCh += array.get(key[j]);
                if (((keyCh > (int) 'z') && lang.equals("eng")) || ((keyCh > (int) 'я') && lang.equals("rus"))) {
                    keyCh -= alphabetSize - 1;
                }
            }
            ++j;
            if(j >= keyLength) {
                j = 0;
            }
            result.append((char) (keyCh));
        }
        return new HashMap<String, String>() {{
            put("result", result.toString());
            put("status", "1");
        }};
    }

    @PostMapping("/decrypt")
    public Map<String, String> decrypt(@RequestBody Map<String, String> textAndKey)  {
        String keyString = textAndKey.get("key").trim().toLowerCase();
        char[] key = keyString.toCharArray();
        String textString = textAndKey.get("text").trim().toLowerCase().replaceAll("[\\s,;.-]","");
        char[] text = textString.toCharArray();
        if(text.length == 0) {
            return new HashMap<String, String>() {{
                put("message", "Введите текст");
                put("status", "0");
            }};
        }
        String lang = textAndKey.get("lang");

        StringBuilder result = new StringBuilder();
        HashMap<Character, Integer> array = null;
        int alphabetSize = 0;
        if(lang.equals("rus")) {
            alphabetSize = CryptHelper.russian.length();
            if(!textAndKey.get("key").trim().toLowerCase().matches("[а-я]+")) {
                return new HashMap<String, String>() {{
                    put("message", "Введите ключ на русском языке");
                    put("status", "0");
                }};
            }
            array = CryptHelper.russianHashMap;
        } else {
            alphabetSize = CryptHelper.english.length();
            if(!textAndKey.get("key").trim().toLowerCase().matches("[a-z]+")) {
                return new HashMap<String, String>() {{
                    put("message", "Введите ключ на английском языке");
                    put("status", "0");
                }};
            }
            array = CryptHelper.englishHashMap;
        }
        int keyLength = key.length;
        int additionalLength = text.length % keyLength;
        if(additionalLength != 0) {
            int Z = keyLength / additionalLength;
            int ost = keyLength % additionalLength;
            for (int i = 0; i < Z; i++) {
                keyString += keyString;
            }
            keyString += keyString.substring(0, ost);
            key = keyString.toCharArray();
        }
        int j = 0;

        for (char i :
                text) {
            int keyCh = i;
            if (!(String.valueOf(i).matches("[a-z]") && lang.equals("rus"))
                    && !(String.valueOf(i).matches("[а-я]") && lang.equals("eng"))
                    && !Character.isDigit(i)
                    && !Character.isWhitespace(i)) {
                keyCh -= array.get(key[j]);
                if (((keyCh < (int) 'a') && lang.equals("eng")) || ((keyCh < (int) 'а') && lang.equals("rus"))) {
                    keyCh += alphabetSize - 1;
                }
            }
            ++j;
            if(j >= keyLength) {
                j = 0;
            }
            result.append((char) (keyCh));
        }
        return new HashMap<String, String>() {{
            put("result", result.toString());
            put("status", "1");
        }};
    }
}
