package com.cwteam.learing.controller;

import com.cwteam.learing.exception.ValidationException;
import com.cwteam.learing.helpers.CryptHelper;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/permutations")
public class PermutationsController {

    private List<Integer> keys;
    private char[] text;

    public void prepareData(String keyString, String textString) throws ValidationException {
        char[] key = keyString.trim().toCharArray();
        keys = new ArrayList<>();
        for (char i :
                key) {
            if (!Character.isDigit(i) && !Character.isWhitespace(i)) {
                throw new ValidationException("Key must contain only digits");
            } else if (!Character.isWhitespace(i)) {
                int keyPart = Character.digit(i, 10) - 1;
                if (keys.contains(keyPart)) {
                    throw new ValidationException("Digits must be unique");
                }
                keys.add(keyPart);
            }
        }
        textString = textString.replaceAll("\\s","");
        int additional = key.length - textString.length() % key.length;
        StringBuilder temp = new StringBuilder(textString);
        temp.append(" ".repeat(additional));
        textString = temp.toString();
        text = textString.toCharArray();
    }

    @PostMapping("/crypt")
    public Map<String, String> crypt(@RequestBody Map<String, String> textAndKey) {
        String keyString = textAndKey.get("key");
        String textString = textAndKey.get("text");
        try {
            prepareData(keyString, textString);
        } catch (ValidationException e) {
            return new HashMap<>() {{
                put("message", e.getMessage());
                put("status", "0");
            }};
        }
        HashMap<Integer, Integer> temporary = new HashMap<>();
        int k = 0;
        for (int i :
                keys) {
            temporary.put(i, k);
            ++k;
        }

        String result = cipher(temporary);
        return new HashMap<String, String>() {{
            put("result", result);
            put("status", "1");
        }};
    }

    private String cipher(HashMap<Integer, Integer> keymap) {
        StringBuilder result = new StringBuilder();
        int j = 0;
        int keyLength = keys.size();
        int intKeys = 0;
        for (int i = 0; i < text.length; i++, j++) {
            if (j == keyLength) {
                j = 0;
                intKeys++;
            }
            result.append(text[keymap.get(j) + intKeys * keyLength]);
        }
        return result.toString();
    }

    @PostMapping("/decrypt")
    public Map<String, String> decrypt(@RequestBody Map<String, String> textAndKey) {
        String keyString = textAndKey.get("key");
        String textString = textAndKey.get("text");
        try {
            prepareData(keyString, textString);
        } catch (ValidationException e) {
            return new HashMap<>() {{
                put("message", e.getMessage());
                put("status", "0");
            }};
        }
        HashMap<Integer, Integer> temporary = new HashMap<>();
        int k = 0;
        for (int i :
                keys) {
            temporary.put(k, i);
            ++k;
        }

        String result = cipher(temporary);
        return new HashMap<>() {{
            put("result", result);
            put("status", "1");
        }};
    }

    @PostMapping("/attack")
    private Map<String, String> attack(@RequestBody Map<String, String> requestData) {
        HashMap<String, HashMap<String, String>> table = CryptHelper.getProbLogRusTable();
        
        return new HashMap<>() {{
            put("result", "r");
            put("status", "1");
        }};
    }

}
