package com.cwteam.learing.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class HomeController {
    @GetMapping("/caesar")
    public String caesar() {
        return "caesar";
    }

    @GetMapping("/replace")
    public String replace() {
        return "replace";
    }

    @GetMapping("/vigenere")
    public String vigenere() {
        return "vigenere";
    }
    @GetMapping("/permutations")
    public String permutations() {
        return "permutations";
    }

    @GetMapping("/aphenaf")
    public String aphenaf() {
        return "aphenaf";
    }

    @GetMapping("/aphenak")
    public String aphenak() {
        return "aphenak";
    }


}


