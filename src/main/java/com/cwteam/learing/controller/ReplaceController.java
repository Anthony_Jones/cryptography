package com.cwteam.learing.controller;

import com.cwteam.learing.exception.ReplaceKeyException;
import com.cwteam.learing.helpers.CryptHelper;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/replace")
public class ReplaceController {


    @PostMapping("/crypt")
    public Map<String, String> crypt(@RequestBody Map<String, String> textAndKey) throws ReplaceKeyException {
        char[] keyCharArray = textAndKey.get("key").trim().toLowerCase().toCharArray();
        char[] text = textAndKey.get("text").trim().toLowerCase().toCharArray();
        String lang = textAndKey.get("lang");

        String tempKey = null;
        String tempAlphabet = null;
        if(lang.equals("rus")) {
            tempKey = textAndKey.get("key").trim().toLowerCase();
            tempAlphabet = CryptHelper.russian;
            for (char i :
                    tempKey.toCharArray()) {
                tempAlphabet = tempAlphabet.replaceAll(String.valueOf(i),"");
            }
        } else {
            tempKey = textAndKey.get("key").trim().toLowerCase();
            tempAlphabet = CryptHelper.english;
            for (char i :
                    tempKey.toCharArray()) {
                tempAlphabet = tempAlphabet.replaceAll(String.valueOf(i),"");
            }
        }
        if(tempAlphabet.length() != 0) {
            String message = "Некоректный ключ";
            return new HashMap<String, String>() {{
                put("crypted_text", message);
            }};
        }


        Map<Character, Character> key = new HashMap<>();
        if (lang.equals("rus") && keyCharArray.length != 33 || lang.equals("eng") && keyCharArray.length != 26) {
            return new HashMap<String, String>() {{
                put("crypted_text", "Key does not match to alphabet");
            }};
        } else {
            char[] alphabet;
            if (lang.equals("rus")) {
                alphabet = CryptHelper.russianArray;
            } else {
                alphabet = CryptHelper.englishArray;
            }
            for (int i = 0; i < keyCharArray.length; i++) {
                key.put(alphabet[i], keyCharArray[i]);
            }
        }
        StringBuilder result = new StringBuilder();
        for (char i :
                text) {
            if (key.get(i) != null) {
                result.append(key.get(i));
            }
        }
        return new HashMap<String, String>() {{
            put("crypted_text", result.toString());
        }};
    }
    private String decrypt(char[] text, char[] keyCharArray, String lang) throws ReplaceKeyException {
        Map<Character, Character> key = new HashMap<>();
        if (lang.equals("rus") && keyCharArray.length != 33 || lang.equals("eng") && keyCharArray.length != 26) {
            throw new ReplaceKeyException("Key does not match to alphabet");
        } else {
            char[] alphabet;
            if (lang.equals("rus")) {
                alphabet = CryptHelper.russianArray; //RUSSIAN "a"
            } else {
                alphabet = CryptHelper.englishArray; //ENGLISH "a"
            }
            for (int i = 0; i < keyCharArray.length; i++) {
                key.put(keyCharArray[i], alphabet[i]);
            }
        }
        StringBuilder result = new StringBuilder();
        for (char i :
                text) {
            if (key.get(i) != null) {
                result.append(key.get(i));
            }
        }
        return result.toString();
    }
    @PostMapping("/decrypt")
    public Map<String, String> decrypt(@RequestBody Map<String, String> textAndKey) throws ReplaceKeyException {
        char[] keyCharArray = textAndKey.get("key").trim().toLowerCase().toCharArray();
        char[] text = textAndKey.get("text").trim().toLowerCase().toCharArray();
        String lang = textAndKey.get("lang");
        String result = decrypt(text, keyCharArray, lang);
        return new HashMap<String, String>() {{
            put("decrypted_text", result);
        }};
    }

    @PostMapping("/freq")
    public Map<Character, Double> freq(@RequestBody Map<String, String> textAndKey) throws Exception {
        String text = textAndKey.get("text");
        String lang = textAndKey.get("lang");
        return CryptHelper.frequency(text, lang);
    }

    @PostMapping("/attack")
    private Map<String, String> attack(@RequestBody Map<String, String> textAndKey) throws ReplaceKeyException {
        char[] text = textAndKey.get("text").trim().toLowerCase().toCharArray();
        String lang = textAndKey.get("lang");
        Map<Character, Double> frequency = CryptHelper.frequency(textAndKey.get("key").trim().toLowerCase(), textAndKey.get("lang").trim());
        int size = 0;
        char[] freqTable;
        if (lang.equals("rus")) {
            size = 33;
            freqTable = CryptHelper.getFrequencyTableRusDesc();
        } else {
            size = 26;
            freqTable = CryptHelper.getFrequencyTableEngDesc();
        }
        int j = 0;
        char[] keyCharArray = new char[size];
        for (Map.Entry<Character, Double> i :
                frequency.entrySet()) {
            keyCharArray[j] = i.getKey();
            ++j;
        }
        Map<Character, Character> key = new HashMap<>();
        for (int i = 0; i < keyCharArray.length; i++) {
            key.put(keyCharArray[i], freqTable[i]);
        }
        StringBuilder result = new StringBuilder();
        for (char i :
                text) {
            if (key.get(i) != null) {
                result.append(key.get(i));
            }
        }
        return new HashMap<>() {{
           put("attackted_text", result.toString());
        }};
    }
}
