package com.cwteam.learing.controller;

import com.cwteam.learing.exception.ValidationException;
import com.cwteam.learing.helpers.CryptHelper;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.io.*;

import static com.cwteam.learing.helpers.CryptHelper.*;

@RestController
@RequestMapping("/aphenaf")
public class AphenafController {


    private char[] alphabet;
    private int alphabetSize;
    private HashMap<Character, Integer> alphabetHashMap;

    private LinkedHashMap<Integer,ArrayList<Integer>> keysRus = new LinkedHashMap<>() {{
        ArrayList<Integer> keyB = new ArrayList<>() {{
            for (int i = 0; i < RusSize; ++i) {
                add(i);
            }
        }};
        for (Integer i :
                CryptHelper.RusSimpleDigits) {
                put(i,keyB);
        }
    }};

    private LinkedHashMap<Integer,ArrayList<Integer>> keysEng = new LinkedHashMap<>() {{
        ArrayList<Integer> keyB = new ArrayList<>() {{
            for (int i = 0; i < EngSize; ++i) {
                add(i);
            }
        }};
        for (Integer i :
                CryptHelper.EngSimpleDigits) {
            put(i,keyB);
        }
    }};


    public void checkData(int keyA, int keyB, String textString, String lang) throws ValidationException {
        if (textString.isEmpty()) {
            throw new ValidationException("Enter text");
        }
        if (lang.equals("rus")) {
            alphabetSize = RusSize;
            alphabet = russianArray;
            alphabetHashMap = russianHashMap;
        } else {
            alphabetSize = EngSize;
            alphabet = englishArray;
            alphabetHashMap = englishHashMap;
        }
        if (lang.equals("rus") && !RusSimpleDigits.contains(keyA)
                || lang.equals("eng") && !EngSimpleDigits.contains(keyA)) {
            throw new ValidationException("Ключ а должен быть взаимнопростым к числу " + alphabetSize);
        }
        if (keyB < 0 || keyB > alphabetSize - 1) {
            throw new ValidationException("Key b should be in range [0, " + alphabetSize + ")");
        }

    }

    private HashMap<String, String> message(String text) {
        return new HashMap<>() {{
            put("message", text);
            put("status", "0");
        }};
    }

    private HashMap<String, String> message(String text, boolean status) {
        return new HashMap<>() {{
            put("message", text);
            put("status", status ? "1" : "0");
        }};
    }

    @PostMapping("/crypt")
    public Map<String, String> crypt(@RequestBody Map<String, String> textAndKey) throws ValidationException {
        String text = textAndKey.get("text").trim().replaceAll("\\s+","");
        String language = textAndKey.get("lang");
        int keyA = Integer.parseInt(textAndKey.get("keya"));
        int keyB = Integer.parseInt(textAndKey.get("keyb"));
        try {
            checkData(keyA, keyB, text, language);
        } catch (ValidationException e) {
            return message(e.getMessage());
        }
        String result = cipher(text,keyA, keyB);

        return new HashMap<String, String>() {{
            put("result", result);
            put("status", "1");
        }};
    }

    @PostMapping("/decrypt")
    public Map<String, String> decrypt(@RequestBody Map<String, String> textAndKey) {
        String text = textAndKey.get("text").trim().replaceAll("\\s+","");
        String language = textAndKey.get("lang");
        int keyA = Integer.parseInt(textAndKey.get("keya"));
        int keyB = Integer.parseInt(textAndKey.get("keyb"));
        try {
            checkData(keyA, keyB, text, language);
        } catch (ValidationException e) {
            return message(e.getMessage());
        }
        String result = decipher(text,keyA,keyB);

        return new HashMap<String, String>() {{
            put("result", result);
            put("status", "1");
        }};
    }

    @PostMapping("/attack")
    private Map<String, String> attack(@RequestBody Map<String, String> requestData) {
        String language = requestData.get("lang");
        if (language.equals("rus")) {
            alphabetSize = RusSize;
            alphabet = russianArray;
            alphabetHashMap = russianHashMap;
        } else {
            alphabetSize = EngSize;
            alphabet = englishArray;
            alphabetHashMap = englishHashMap;
        }
        String text = requestData.get("text").replaceAll("\\s+","").toLowerCase();
        if(text.isEmpty()) {
            return message("Enter text");
        }

        double maxHi = 0;
        if(language.equals("rus")) {
            Map<Character, Double> frequency= frequencyTableRus;
        } else  {
            Map<Character, Double> frequency= frequencyTableEng;
        }
        Integer resultKeyA = 0;
        Integer resultKeyB = 0;
        for(Map.Entry<Integer,ArrayList<Integer>> entry : keysRus.entrySet()) {
            Integer key = entry.getKey();
            ArrayList<Integer> value = entry.getValue();
            double Xi = 0;
            for (Integer k :
                    value) {
                Map<Character, Double> attemptFrequency = frequency(decipher(text,key,k),language);
                for(Map.Entry<Character, Double> entryJ :
                        frequencyTableRus.entrySet()) {
                    Character alpha = entryJ.getKey();
                    Double freq = entryJ.getValue();
                    Double alphaFreq = 0.0;
                    alphaFreq  = attemptFrequency.get(alpha);
                    if(alphaFreq != null) {
                        Xi += freq * alphaFreq;
                    }
                }
                if(Xi > maxHi ) {
                    resultKeyA = key;
                    resultKeyB = k;
                    maxHi = Xi;
                }
            }
        }
        Integer finalResultKeyA = resultKeyA;
        Integer finalResultKeyB = resultKeyB;
        return new HashMap<>() {{
            put("result", decipher(text, finalResultKeyA, finalResultKeyB));
            put("status", "1");
        }};
    }

    private String cipher(String text, Integer keyA, Integer keyB) {
        StringBuilder result = new StringBuilder();
        for (char i :
                text.toCharArray()) {
            if (alphabetHashMap.get(i) == null) {
                result.append(i);
            } else {
                result.append(alphabet[(alphabetHashMap.get(i) * keyA + keyB) % alphabetSize]);
            }
        }
        return result.toString();
    }
    private String decipher(String text, Integer keyA, Integer keyB) {
        int inv = CryptHelper.inv(keyA, alphabetSize);
        StringBuilder result = new StringBuilder();
        for (char i :
                text.toCharArray()) {
            if (alphabetHashMap.get(i) == null) {
                result.append(i);
            } else {
                int temp = alphabetHashMap.get(i) > keyB ? 0 : alphabetSize;
                result.append(alphabet[inv * (alphabet[i] + temp - keyB) % alphabetSize]);
            }
        }
        return result.toString();
    }
}
