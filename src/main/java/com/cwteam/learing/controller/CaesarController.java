package com.cwteam.learing.controller;

import com.cwteam.learing.helpers.CryptHelper;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/caesar")
public class CaesarController {

    final private int EngSize = 26;
    final public int RusSize = 33;



    @PostMapping("/attack")
    private Map<Integer, String> attack(@RequestBody Map<String, String> requestData) {
        String textStr = requestData.get("text");
        String language = requestData.get("lang");
        Map<Integer, String> result = new HashMap<>();
        Map<Character, Double> frequency = CryptHelper.frequency(textStr, language);
        int i = 0;
        int max = 10;
        Map<Character, Double> freqTable;
        if (language.equals("rus")) {
            freqTable = CryptHelper.frequencyTableRus;
        } else {
            freqTable = CryptHelper.frequencyTableEng;
        }
        for (Map.Entry<Character, Double> j :
                frequency.entrySet()) {
            for (Map.Entry<Character, Double> k :
                    freqTable.entrySet()) {
                if (i > max) {
                    break;
                }
                int key = (int) j.getKey() - (int) k.getKey();
                if (key < 0) {
                    key *= -1;
                }
                result.put(key, decrypt(textStr, key, language));
                ++i;
            }
            break;
        }
        return result;
    }



    private String cryptTxt(String string, Integer key, String lang) throws Exception {
        int alphabetSize = 0;
        String str = string.replaceAll("\\s+", "");
        if (lang.equals("rus")) {
            alphabetSize = RusSize;
        } else {
            alphabetSize = EngSize;
        }
        if (key < 1 || key > alphabetSize - 1) {
            throw new Exception("Please enter in bounds (1, " + (alphabetSize - 1) + ")");
        }
        char[] str1 = str.toCharArray();
        StringBuilder result = new StringBuilder();

        for (char i :
                str1) {
            int keyCh = i;
            if (!(String.valueOf(i).matches("[A-Za-z]") && lang.equals("rus"))
                    && !(String.valueOf(i).matches("[А-Яа-я]") && lang.equals("eng"))
                    && !Character.isDigit(i)
                    && !Character.isWhitespace(i)) {
                keyCh += key;
                if (((keyCh > (int) 'z') && lang.equals("eng")) || ((keyCh > (int) 'я') && lang.equals("rus"))) {
                    keyCh -= alphabetSize - 1;
                }
            }
            result.append((char) (keyCh));
        }
        return result.toString();
    }

    private String decrypt(String string, Integer key, String lang) {
        int alphabetSize = 0;
        String str = string.replaceAll("\\s+", "");
        if (lang.equals("rus")) {
            alphabetSize = RusSize;
        } else {
            alphabetSize = EngSize;
        }
        char[] str1 = str.toCharArray();
        StringBuilder result = new StringBuilder();

        for (char i :
                str1) {

            int keyCh = i;

            if (!(String.valueOf(i).matches("[A-Za-z]") && lang.equals("rus"))
                    && !(String.valueOf(i).matches("[А-Яа-я]") && lang.equals("eng"))
                    && !Character.isDigit(i)) {
                keyCh -= key;
                if ((keyCh < (int) 'a' && lang.equals("eng")) || (keyCh < (int) 'а' && lang.equals("rus"))) {
                    keyCh += alphabetSize - 1;
                }
            }
            result.append((char) (keyCh));
        }
        return result.toString();
    }

    @PostMapping("/crypt")
    public Map<String, String> crypt(@RequestBody Map<String, String> textAndKey) throws Exception {
        String text = textAndKey.get("text");
        Integer key = Integer.parseInt(textAndKey.get("key"));
        String lang = textAndKey.get("lang");
        return new HashMap<>() {{
            put("crypted_text", cryptTxt(text, key, lang));
        }};
    }

    @PostMapping("/decrypt")
    public Map<String, String> decrypt(@RequestBody Map<String, String> textAndKey) throws Exception {
        String text = textAndKey.get("text");
        Integer key = Integer.parseInt(textAndKey.get("key"));
        String lang = textAndKey.get("lang");
        return new HashMap<>() {{
            put("decrypted_text", decrypt(text, key, lang));
        }};
    }

    @PostMapping("/freq")
    public Map<Character, Double> freq(@RequestBody Map<String, String> textAndKey) throws Exception {
        String text = textAndKey.get("text");
        String lang = textAndKey.get("lang");
        return CryptHelper.frequency(text, lang);
    }
}
