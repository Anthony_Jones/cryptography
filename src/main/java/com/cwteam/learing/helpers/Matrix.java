package com.cwteam.learing.helpers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class Matrix<Type> implements Iterable<Type>{

    private HashMap<Integer,HashMap<Integer,Type>> matrix;

    Matrix() {
        matrix = new HashMap<>();
    }


    public Matrix(ArrayList<Type> numbers, int rows, int columns) {
        matrix = new HashMap<>();
        for (int i = 0; i < rows; i++) {
            HashMap<Integer,Type> row = new HashMap<>();
            for (int j = 0; j < columns; j++) {
                System.out.println(i * columns + j);
                row.put(j,numbers.get(i * columns + j));
            }
            matrix.put(i,row);
        }
    }

    public Matrix(int rows, int columns) {
        matrix = new HashMap<>(rows);
        for (int i = 0; i < rows; i++) {
            matrix.put(i,new HashMap<>(columns));
        }
    }

    public void set(Type value, int row, int column) {
        matrix.get(row).put(column, value);
    }
    public Type get(int row, int column) {
        return matrix.get(row).get(column);
    }

    @Override
    public Iterator<Type> iterator() {
        Iterator<Type> it = new Iterator<Type>() {

            private int currentRow = 0;
            private int currentColumn = 0;

            @Override
            public boolean hasNext() {
                return currentRow < matrix.size() && currentColumn < matrix.get(0).size();
            }

            @Override
            public Type next() {
                Type result = matrix.get(currentRow).get(currentColumn);
                currentColumn++;
                if(currentColumn == matrix.get(0).size()) {
                    currentColumn = 0;
                    currentRow++;
                }
                return  result;
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
        return it;
    }

    public int rows() {
        return  matrix.size();
    }
    public int columns() {
        return matrix.get(0).size();
    }

    void setRow(HashMap<Integer, Type> row, int rowNumber)  {
        this.matrix.put(rowNumber,row);
    }
}
