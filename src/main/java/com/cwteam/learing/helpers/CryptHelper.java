package com.cwteam.learing.helpers;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.*;
import java.util.stream.Collectors;

public class CryptHelper {
    public static final ArrayList<Integer> EngSimpleDigits = new ArrayList<>() {{
        add(1);
        add(3);
        add(5);
        add(7);
        add(9);
        add(11);
        add(15);
        add(17);
        add(19);
        add(21);
        add(23);
        add(25);
    }};
    public static   Integer inv(int a, int b) {
        for (var i = 1; i < b; i++) {
            if ((a * i) % b == 1)
                return i;
        }
        return 1;
    }
    public static final ArrayList<Integer> RusSimpleDigits = new ArrayList<>() {{
        add(1);
        add(2);
        add(4);
        add(5);
        add(7);
        add(8);
        add(10);
        add(13);
        add(14);
        add(16);
        add(17);
        add(19);
        add(20);
        add(20);
        add(23);
        add(25);
        add(26);
        add(28);
        add(29);
        add(31);
        add(32);
    }};
    final static public Map<Character, Double> frequencyTableEng = new LinkedHashMap<>() {{
        put('e', 0.127);
        put('t', 0.097);
        put('i', 0.075);
        put('a', 0.073);
        put('o', 0.068);
        put('n', 0.067);
        put('s', 0.067);
        put('r', 0.064);
        put('h', 0.049);
        put('c', 0.045);
        put('l', 0.040);
        put('d', 0.031);
        put('p', 0.030);
        put('y', 0.027);
        put('u', 0.024);
        put('m', 0.024);
        put('f', 0.021);
        put('b', 0.017);
        put('g', 0.016);
        put('w', 0.013);
        put('v', 0.008);
        put('k', 0.008);
        put('x', 0.005);
        put('q', 0.002);
        put('z', 0.001);
        put('j', 0.001);
    }};
    static public Map<Character, Double> frequencyTableRus = new LinkedHashMap<>() {{
        put('и', 0.175);
        put('о', 0.090);
        put('е', 0.072);
        put('ё', 0.072);
        put('а', 0.062);
        put('н', 0.053);
        put('т', 0.053);
        put('с', 0.045);
        put('р', 0.040);
        put('в', 0.038);
        put('л', 0.035);
        put('к', 0.028);
        put('м', 0.026);
        put('д', 0.025);
        put('п', 0.023);
        put('у', 0.021);
        put('я', 0.018);
        put('з', 0.016);
        put('ы', 0.016);
        put('б', 0.014);
        put('ь', 0.014);
        put('ъ', 0.014);
        put('г', 0.013);
        put('ч', 0.012);
        put('й', 0.010);
        put('х', 0.009);
        put('ж', 0.007);
        put('ш', 0.006);
        put('ю', 0.006);
        put('ц', 0.004);
        put('щ', 0.003);
        put('э', 0.003);
        put('ф', 0.002);
    }};

    final static public char[] russianArray = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя".toCharArray();
    final static public String russian = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
    final static public char[] englishArray = "abcdefghijklmnopqrstuvwxyz".toCharArray();
    final static public String english = "abcdefghijklmnopqrstuvwxyz";
    final static public HashMap<Character, Integer> englishHashMap = new HashMap<>() {{
        int j = 0;
        for (char i :
                englishArray) {
            put(i,j);
            ++j;
        }
    }};
    final static public HashMap<Character, Integer> russianHashMap = new HashMap<>() {{
        int j = 0;
        for (char i :
                russianArray) {
            put(i,j);
            ++j;
        }
    }};

    static public Map<Character, Double> frequency(String string, String lang) {
        Map<Character, Double> result = new HashMap<>();

        String s = string.replaceAll("", "").toLowerCase();
        if (lang.equals("rus")) {
            s = s.replaceAll("[^а-я]", "");
        } else {
            s = s.replaceAll("[^a-z]", "");
        }
        char[] text = s.toCharArray();
        int count = 0;
        for (char j : text) {
            count = 0;
            for (char i : text) {
                if (i == j) {
                    ++count;
                }
            }
            result.put(j, (double) count / s.length());
        }
        result = result.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(Collections.reverseOrder()))
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new
                ));
        return result;
    }

    public static char[] getFrequencyTableEngDesc() {
        char[] result = new char[26];
        int j = 0;
        for (Map.Entry<Character, Double> i :
                frequencyTableEng.entrySet()) {
            result[j] = i.getKey();
            ++j;
        }
        return result;
    }
    public static char[] getFrequencyTableRusDesc() {
        char[] result = new char[33];
        int j = 0;
        for (Map.Entry<Character, Double> i :
                frequencyTableRus.entrySet()) {
            result[j] = i.getKey();
            ++j;
        }
        return result;
    }

    public static HashMap<String, HashMap<String, String>> getProbLogRusTable() {
        FileReader logTableReader;
        try {
            logTableReader = new FileReader("src/main/resources/logtable/rustable.txt");
        } catch (FileNotFoundException e) {
            return null;
        }
        Scanner scanner = new Scanner(logTableReader);

        HashMap<String, HashMap<String, String>> result = new HashMap<>();
        String letters = scanner.nextLine();
        String[] lettersArray = letters.split("\\s+");
        while (scanner.hasNext()) {
            HashMap<String, String> row = new HashMap<>();
            String[] rowArray = scanner.nextLine().split("\\s+");
            for (int i = 1; i < rowArray.length; i++) {
                row.put(lettersArray[i], rowArray[i]);
            }
            result.put(rowArray[0], row);
        }

        return result;
    }

    final public static int EngSize = 26;
    final public static int RusSize = 33;

}
