package com.cwteam.learing.helpers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class MatrixDouble implements Iterable<Double>{
    Matrix<Double> matrix;

    public MatrixDouble(Matrix<Double> matrix) {
        this.matrix = matrix;
    }
    public MatrixDouble() {
        matrix = new Matrix<>();
    }
    public MatrixDouble(ArrayList<Double> data, int rows, int columns) {
        matrix = new Matrix<>(data, rows, columns);
    }
    public Double det() {
        return matrix.get(0,0) * matrix.get(1,1) - matrix.get(0,1) * matrix.get(1,0);
    }
    public MatrixDouble mul(MatrixDouble right)   {
        MatrixDouble result = new MatrixDouble();
        for (int i = 0; i < this.matrix.rows(); i++) {
            HashMap<Integer,Double> row = new HashMap<>();
            for (int j = 0; j < right.matrix.columns(); j++) {
                double resultIJ = 0.0;
                for (int k = 0; k < this.matrix.columns(); k++) {
                    resultIJ +=  this.matrix.get(i,k) * right.matrix.get(k,j) ;
                }
                row.put(j,resultIJ);
            }
            result.matrix.setRow(row, i );
        }
        return result;
    }
    public MatrixDouble(int rows, int columns) {
        matrix = new Matrix<>(rows, columns);
    }
    public MatrixDouble inv(int alphabetSize) {
        int inv = CryptHelper.inv(det().intValue(),alphabetSize);
        ArrayList<Double> temp = new ArrayList<>() {{
            add((matrix.get(1,1)*inv) % alphabetSize);
            add((-matrix.get(0,1)*inv) % alphabetSize);
            add((-matrix.get(1,0)*inv) % alphabetSize);
            add((matrix.get(0,1)*inv) % alphabetSize);
        }};
        for (int i = 0; i < temp.size(); i++) {
            temp.set(i,temp.get(i) * ((temp.get(i) < 0) ? -1: 1));
        }
        return new MatrixDouble(temp,2,2);
    }
    public void set(Double value, int row, int column) {
        matrix.set(value, row, column);
    }
    public Iterator<Double> iterator() {
        return matrix.iterator();
    }

    public int columns() {
        return matrix.columns();
    }

    public int rows() {
        return matrix.rows();
    }

    public Double get(int row, int column) {
        return matrix.get(row, column);
    }
}

