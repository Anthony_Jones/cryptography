package com.cwteam.learing.exception;

public class ReplaceKeyException extends Exception{

    public ReplaceKeyException(String s) {
        super(s);
    }
}
